# Founders Map
> Coding challenge for [TryCatch.tech](http://trycatch.tech/)


## Instalation

Install dependencies:

    npm install


# Development
---

## Develop

To run development server use:

    npm start


## DevTools
To toggle DevTools in development mode use:

Toggle visibility

    ctrl + h

Change position
    
    ctrl + g


# Production
---

## Build
To run production bundle build use:

    npm run build


## Server
To run production server use:

    npm run server


## License
Copyright (c) 2016 **Matko Bulic**.  
Licensed under the **MIT** license.
