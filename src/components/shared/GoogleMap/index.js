/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Google Map - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { connect } from 'react-redux';

import GoogleMap from './GoogleMap';

import { getMapMarkers } from '../../../selectors';

/**
 *  Connect
 */
export default connect(
  state => ({ markers: getMapMarkers(state) })
)(GoogleMap);
