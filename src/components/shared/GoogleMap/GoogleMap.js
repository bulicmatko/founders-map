/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Google Map

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import cssModules from 'react-css-modules';
import debounce from 'lodash/debounce';

import { mapConfig, mapStyles } from './settings';

import styles from './styles';

const { google } = window;

/**
 *  Google Map
 */
@cssModules(styles)
export default class extends Component {
  static displayName = 'GoogleMap';

  static propTypes = {
    markers: PropTypes.arrayOf(
      PropTypes.shape({
        lat: PropTypes.number.isRequired,
        lng: PropTypes.number.isRequired,
        label: PropTypes.node.isRequired,
      })
    ).isRequired,
  };

  static defaultProps = {
    markers: [],
  };

  componentDidMount() {
    const container = findDOMNode(this.refs.GoogleMap);

    this.googleMap = new google.maps.Map(container, mapConfig);

    this.googleMap.set('styles', mapStyles);

    this.markers = [];

    this.renderMarkers();

    this.handleResize = debounce(this.renderMarkers, 300);

    window.addEventListener('resize', this.handleResize, 0);
  }

  componentDidUpdate() {
    this.renderMarkers();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  removeMarkers() {
    this.markers.forEach(marker => {
      marker.setMap(null);
    });

    this.markers = [];
  }

  renderMarkers = () => {
    this.removeMarkers();

    const { markers } = this.props;
    const bounds = new google.maps.LatLngBounds();

    markers.forEach(marker => {
      const { lat, lng, label } = marker;

      const googleMarker = new google.maps.Marker({
        map: this.googleMap,
        position: { lat, lng },
        title: label,
      });

      const infowindow = new google.maps.InfoWindow({
        content: label,
      });

      googleMarker.addListener('click', () => {
        infowindow.open(this.googleMap, googleMarker);
      });

      this.markers.push(googleMarker);

      bounds.extend(new google.maps.LatLng(lat, lng));
    });

    this.googleMap.fitBounds(bounds);
    const zoom = this.googleMap.getZoom() - 1;
    this.googleMap.setZoom(zoom > 17 ? 17 : zoom);
  }

  render() {
    return (
      <div styleName="GoogleMap">
        <div styleName="GoogleMap--Container" ref="GoogleMap" />
      </div>
    );
  }
}
