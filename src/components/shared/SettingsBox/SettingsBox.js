/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Settings Box - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';
import cssModules from 'react-css-modules';
import noop from 'lodash/noop';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import styles from './styles';

/**
 *  Settings Box
 */
@cssModules(styles)
export default class extends Component {
  static displayName = 'SettingsBox';

  static propTypes = {
    delimiterOptions: PropTypes.array.isRequired,
    delimiter: PropTypes.string.isRequired,
    onDelimiterChange: PropTypes.func.isRequired,
    latitudeColumnOptions: PropTypes.array.isRequired,
    latitudeColumn: PropTypes.string.isRequired,
    onLatitudeColumnChange: PropTypes.func.isRequired,
    longitudeColumnOptions: PropTypes.array.isRequired,
    longitudeColumn: PropTypes.string.isRequired,
    onLongitudeColumnChange: PropTypes.func.isRequired,
    markerLabelColumnOptions: PropTypes.array.isRequired,
    markerLabelColumn: PropTypes.string.isRequired,
    onMarkerLabelColumnChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    delimiterOptions: [],
    delimiter: '',
    onDelimiterChange: noop,
    latitudeColumnOptions: [],
    latitudeColumn: '',
    onLatitudeColumnChange: noop,
    longitudeColumnOptions: [],
    longitudeColumn: '',
    onLongitudeColumnChange: noop,
    markerLabelColumnOptions: [],
    markerLabelColumn: '',
    onMarkerLabelColumnChange: noop,
  };

  renderSelector(label, options, value, onChange) {
    return (
      <SelectField
        fullWidth
        value={value}
        floatingLabelText={label}
        onChange={(e, i, v) => onChange(v)}
      >
        {options.map((option, index) => (
          <MenuItem
            key={`${option.value}-${index}`}
            value={option.value}
            primaryText={option.name}
          />
        ))}
      </SelectField>
    );
  }

  render() {
    const {
      delimiterOptions, delimiter, onDelimiterChange,
      latitudeColumnOptions, latitudeColumn, onLatitudeColumnChange,
      longitudeColumnOptions, longitudeColumn, onLongitudeColumnChange,
      markerLabelColumnOptions, markerLabelColumn, onMarkerLabelColumnChange,
    } = this.props;

    return (
      <div styleName="SettingsBox">
        <div styleName="row">
          <div styleName="col">
            {this.renderSelector(
              'Delimiter',
              delimiterOptions,
              delimiter,
              onDelimiterChange
            )}
          </div>
          <div styleName="col">
            {this.renderSelector(
              'Latitude Column',
              latitudeColumnOptions,
              latitudeColumn,
              onLatitudeColumnChange
            )}
          </div>
          <div styleName="col">
            {this.renderSelector(
              'Longitude Column',
              longitudeColumnOptions,
              longitudeColumn,
              onLongitudeColumnChange
            )}
          </div>
          <div styleName="col">
            {this.renderSelector(
              'Marker Label Column',
              markerLabelColumnOptions,
              markerLabelColumn,
              onMarkerLabelColumnChange
            )}
          </div>
        </div>
      </div>
    );
  }
}
