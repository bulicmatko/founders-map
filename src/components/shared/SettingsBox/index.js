/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Settings Box - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { connect } from 'react-redux';

import SettingsBox from './SettingsBox';

import { getSettings } from '../../../selectors';

import changeDelimiter from '../../../actions/changeDelimiter.action';
import changeLatitudeColumn from '../../../actions/changeLatitudeColumn.action';
import changeLongitudeColumn from '../../../actions/changeLongitudeColumn.action';
import changeMarkerLabelColumn from '../../../actions/changeMarkerLabelColumn.action';

/**
 *  Connect
 */
export default connect(
  state => getSettings(state),
  dispatch => ({
    onDelimiterChange: delimiter => {
      dispatch(changeDelimiter(delimiter));
    },
    onLatitudeColumnChange: latitudeColumn => {
      dispatch(changeLatitudeColumn(latitudeColumn));
    },
    onLongitudeColumnChange: longitudeColumn => {
      dispatch(changeLongitudeColumn(longitudeColumn));
    },
    onMarkerLabelColumnChange: markerLabelColumn => {
      dispatch(changeMarkerLabelColumn(markerLabelColumn));
    },
  })
)(SettingsBox);
