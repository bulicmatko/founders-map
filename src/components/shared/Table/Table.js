/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Table - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';
import cssModules from 'react-css-modules';
import without from 'lodash/without';
import union from 'lodash/union';
import noop from 'lodash/noop';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Checkbox from 'material-ui/Checkbox';

import { isUrl, isImageUrl } from '../../../utils/parseString.utils';

import styles from './styles';

/**
 *  Table
 */
@cssModules(styles)
export default class extends Component {
  static displayName = 'Table';

  static propTypes = {
    columns: PropTypes.arrayOf(
      PropTypes.string
    ).isRequired,
    data: PropTypes.arrayOf(
      PropTypes.object
    ).isRequired,
    order: PropTypes.object.isRequired,
    filter: PropTypes.object.isRequired,
    hiddenRowIds: PropTypes.array.isRequired,
    onTableOrderChange: PropTypes.func.isRequired,
    onTableFilterChange: PropTypes.func.isRequired,
    onRowOnMapVisabilityChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    columns: [],
    data: [],
    order: {},
    filter: {},
    hiddenRowIds: [],
    onTableOrderChange: noop,
    onTableFilterChange: noop,
    onRowOnMapVisabilityChange: noop,
  };

  handleOrderChange(e, column) {
    e.preventDefault();

    const { order } = this.props;
    const direction = order[column] === 'asc' ? 'desc' : 'asc';

    this.props.onTableOrderChange({ [column]: direction });
  }

  handleRowOnMapVisabilityChange = (rowId, isChecked) => {
    let { hiddenRowIds } = this.props;

    if (isChecked) {
      hiddenRowIds = union(hiddenRowIds, [rowId]);
    } else {
      hiddenRowIds = without(hiddenRowIds, rowId);
    }

    this.props.onRowOnMapVisabilityChange(hiddenRowIds);
  };

  renderTableHeaderColumn = (column, index) => {
    const { order } = this.props;

    return (
      <TableHeaderColumn key={index}>
        <a href="#" onClick={e => this.handleOrderChange(e, column)}>
          {column}
        </a>
        {order[column] && <small>{` (${order[column]})`}</small>}
      </TableHeaderColumn>
    );
  };

  renderTableColumnContent(content) {
    if (isImageUrl(content)) {
      return (
        <a href={content} target="_blank">
          <img src={content} alt={content} />
        </a>
      );
    }

    if (isUrl(content)) {
      return (
        <a href={content} target="_blank">{content}</a>
      );
    }

    return content;
  }

  renderNoDataInfo() {
    const { data } = this.props;

    if (data.length) {
      return null;
    }

    return (
      <div styleName="empty">
        <h2>No data to display</h2>
      </div>
    );
  }

  render() {
    const { columns, data, filter, hiddenRowIds, onTableFilterChange } = this.props;

    return (
      <div styleName="Table">
        <Table
          wrapperStyle={{ width: 1200 }}
          selectable={false}
          multiSelectable={false}
          style={{ width: '1600px', overflow: 'hidden' }}
          bodyStyle={{ width: '1600px', overflow: 'hidden' }}
          wrapperStyle={{ width: '1600px', overflow: 'hidden' }}
        >
          <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
            enableSelectAll={false}
          >
            <TableRow>
              <TableHeaderColumn>Hide from map</TableHeaderColumn>
              {columns.map(this.renderTableHeaderColumn)}
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
            deselectOnClickaway={false}
          >
            <TableRow>
              <TableRowColumn />
              {columns.map(column => (
                <TableRowColumn key={column}>
                  <input
                    placeholder={`Search ${column}`}
                    value={filter[column] || ''}
                    onChange={e => onTableFilterChange({ ...filter, ...{ [column]: e.target.value } })}
                  />
                </TableRowColumn>
              ))}
            </TableRow>
            {data.length && data.map((row, index) => (
              <TableRow key={index}>
                <TableRowColumn>
                  <Checkbox
                    checked={hiddenRowIds.indexOf(row.Id) > -1}
                    onCheck={(e, isChecked) => this.handleRowOnMapVisabilityChange(row.Id, isChecked)}
                  />
                </TableRowColumn>
                {columns.map(column => (
                  <TableRowColumn key={row[column]}>
                    {this.renderTableColumnContent(row[column])}
                  </TableRowColumn>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {this.renderNoDataInfo()}
      </div>
    );
  }
}
