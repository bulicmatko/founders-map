/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Table - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { connect } from 'react-redux';

import Table from './Table';

import {
  getTableColumns,
  getTableData,
  getTableOrder,
  getTableFilter,
  getHiddenRowIds,
} from '../../../selectors';

import changeTableOrder from '../../../actions/changeTableOrder.action';
import changeTableFilter from '../../../actions/changeTableFilter.action';
import changeRowOnMapVisability from '../../../actions/changeRowOnMapVisability.action';

/**
 *  Connect
 */
export default connect(
  state => ({
    columns: getTableColumns(state),
    data: getTableData(state),
    order: getTableOrder(state),
    hiddenRowIds: getHiddenRowIds(state),
    filter: getTableFilter(state),
  }),
  dispatch => ({
    onTableOrderChange: tableOrder => {
      dispatch(changeTableOrder(tableOrder));
    },
    onTableFilterChange: tableFilter => {
      dispatch(changeTableFilter(tableFilter));
    },
    onRowOnMapVisabilityChange: hiddenRowIds => {
      dispatch(changeRowOnMapVisability(hiddenRowIds));
    },
  })
)(Table);
