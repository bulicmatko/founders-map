/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Home Page

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';
import cssModules from 'react-css-modules';
import { Link } from 'react-router';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import route from '../../../utils/route.util';

import styles from './styles';

/**
 *  Home Page
 */
@cssModules(styles)
export default class extends Component {
  static displayName = 'HomePage';

  static propTypes = {
    data: PropTypes.object.isRequired,
    markers: PropTypes.array.isRequired,
  };

  static defaultProps = {
    data: {},
    markers: {},
  };

  renderWelcomeScreen() {
    const { data } = this.props;

    if (data.csv.length) {
      return null;
    }

    return (
      <Dialog modal open autoScrollBodyContent title="Welcome to Founders Map">
        <div styleName="empty">
          <h2>No data to display</h2>
          <p>Please import your data.</p>
          <br /><br />
          <p>
            <Link to={route('data-import')}>
              <FlatButton label="Import data" primary />
            </Link>
          </p>
        </div>
      </Dialog>
    );
  }

  renderNoDataToDisplayScreen() {
    const { data, markers } = this.props;

    if (!(data.csv.length && !markers.length)) {
      return null;
    }

    return (
      <Dialog modal open autoScrollBodyContent title="Welcome to Founders Map">
        <div styleName="empty">
          <h2>All rows are hidden or data is not mapped correctly</h2>
          <p>Check you data preview</p>
          <br /><br />
          <p>
            <Link to={route('data-preview')}>
              <FlatButton label="Preview data" primary />
            </Link>
          </p>
        </div>
      </Dialog>
    );
  }

  render() {
    return (
      <div styleName="HomePage">
        <nav styleName="navigation">
          <Link to={route('data-import')}>
            <FlatButton label="Import Data" />
          </Link>
          <Link to={route('data-preview')}>
            <FlatButton label="Preview Data" />
          </Link>
        </nav>
        {this.renderWelcomeScreen()}
        {this.renderNoDataToDisplayScreen()}
      </div>
    );
  }
}
