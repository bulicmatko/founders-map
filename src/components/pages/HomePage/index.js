/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Home Page - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { connect } from 'react-redux';

import HomePage from './HomePage';

import { getMapMarkers } from '../../../selectors';


/**
 *  Connect
 */
export default connect(state => ({
  data: state.data,
  markers: getMapMarkers(state),
}))(HomePage);
