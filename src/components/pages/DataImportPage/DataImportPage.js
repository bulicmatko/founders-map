/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Data Import Page

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';
import cssModules from 'react-css-modules';
import { Link } from 'react-router';
import noop from 'lodash/noop';

import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

import route from '../../../utils/route.util';

import styles from './styles';

/**
 *  Data Import Page
 */
@cssModules(styles)
export default class extends Component {
  static displayName = 'DataImportPage';

  static propTypes = {
    csv: PropTypes.string.isRequired,
    onCsvChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    csv: '',
    onCsvChange: noop,
  };

  renderHeader() {
    return (
      <div styleName="header">
        <h2>Import data</h2>
        <p>Enter of Copy/Paste your CSV data</p>
      </div>
    );
  }

  renderContent() {
    const { csv, onCsvChange } = this.props;

    return (
      <div styleName="content">
        <TextField
          multiLine
          fullWidth
          rows={10}
          floatingLabelText="CSV Data"
          value={csv}
          onChange={e => onCsvChange(e.target.value)}
        />
      </div>
    );
  }

  renderFooter() {
    return (
      <div styleName="footer">
        <Link to={route('root')}>
          <FlatButton label="Go to map" />
        </Link>
        <Link to={route('data-preview')}>
          <RaisedButton label="Data preview" primary />
        </Link>
      </div>
    );
  }

  render() {
    return (
      <Dialog modal open autoScrollBodyContent contentStyle={{ maxWidth: 'none' }}>
        {this.renderHeader()}
        {this.renderContent()}
        {this.renderFooter()}
      </Dialog>
    );
  }
}
