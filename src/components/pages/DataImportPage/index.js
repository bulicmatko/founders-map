/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Data Import Page - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { connect } from 'react-redux';

import DataImportPage from './DataImportPage';

import { getCsv } from '../../../selectors';

import changeCsv from '../../../actions/changeCsv.action';

/**
 *  Connect
 */
export default connect(
  state => ({ csv: getCsv(state) }),
  dispatch => ({
    onCsvChange: csv => {
      dispatch(changeCsv(csv));
    },
  })
)(DataImportPage);
