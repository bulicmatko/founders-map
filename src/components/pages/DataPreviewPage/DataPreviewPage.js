/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Data Preview Page

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';
import cssModules from 'react-css-modules';
import { Link } from 'react-router';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

import Table from '../../shared/Table';
import SettingsBox from '../../shared/SettingsBox';

import route from '../../../utils/route.util';

import styles from './styles';

/**
 *  Data Preview Page
 */
@cssModules(styles)
export default class extends Component {
  static displayName = 'DataPreviewPage';

  static propTypes = {
    data: PropTypes.object.isRequired,
  };

  static defaultProps = {
    data: {},
  };

  renderHeader() {
    return (
      <div styleName="header">
        <h2>Data preview</h2>
        <p>Preview data in interactive table</p>
      </div>
    );
  }

  renderContent() {
    const { data } = this.props;

    // if (errors.length) {
    //   return (
    //     <div styleName="error">
    //       <h2>Error</h2>
    //       <p>{errors[0].message}</p>
    //     </div>
    //   );
    // }

    if (!data.csv.length) {
      return (
        <div styleName="empty">
          <h2>No data to display</h2>
          <p>It looks like you do not have any data to display.</p>
          <br /><br />
          <p>
            <Link to={route('data-import')}>
              <FlatButton label="Import data" primary />
            </Link>
          </p>
        </div>
      );
    }

    return (
      <div styleName="content">
        <div styleName="settings">
          <SettingsBox />
        </div>
        <br /><br />
        <Table />
      </div>
    );
  }

  renderFooter() {
    return (
      <div styleName="footer">
        <Link to={route('data-import')}>
          <FlatButton label="Data import" />
        </Link>
        <Link to={route('root')}>
          <RaisedButton label="Go to map" primary />
        </Link>
      </div>
    );
  }

  render() {
    return (
      <Dialog modal open autoScrollBodyContent contentStyle={{ maxWidth: 'none' }}>
        {this.renderHeader()}
        {this.renderContent()}
        {this.renderFooter()}
      </Dialog>
    );
  }
}
