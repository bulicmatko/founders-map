/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Data Preview Page - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { connect } from 'react-redux';

import DataPreviewPage from './DataPreviewPage';

/**
 *  Connect
 */
export default connect(state => state)(DataPreviewPage);
