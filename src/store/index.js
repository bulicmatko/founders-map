/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Store - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./createStore.prod').default(); // eslint-disable-line
} else {
  module.exports = require('./createStore.dev').default(); // eslint-disable-line
}
