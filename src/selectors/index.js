/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Selectors - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { createSelector } from 'reselect';
import orderBy from 'lodash/orderBy';
import includes from 'lodash/includes';
import filter from 'lodash/filter';

import parseCsv from '../utils/parseCsv.util';

/**
 *  Simple Selectors
 */
export const getCsv = state => state.data.csv;
export const getDelimiter = state => state.data.delimiter;
export const getLatitudeColumn = state => state.data.latitudeColumn;
export const getLongitudeColumn = state => state.data.longitudeColumn;
export const getMarkerLabelColumn = state => state.data.markerLabelColumn;
export const getHiddenRowIds = state => state.data.hiddenRowIds;
export const getTableOrder = state => state.data.tableOrder;
export const getTableFilter = state => state.data.tableFilter;

/**
 *  Get Parsed Csv Data Selector
 */
export const getParsedCsv = createSelector(
  getCsv,
  getDelimiter,
  (csv, delimiter) => parseCsv(csv, delimiter)
);

/**
 *  Get Table Data Selector
 */
export const getTableData = createSelector(
  getParsedCsv,
  getTableOrder,
  getTableFilter,
  (parsedCsv, tableOrder, tableFilter) => {
    const orderColumns = Object.keys(tableOrder);
    const orders = orderColumns.map(orderColumn => tableOrder[orderColumn]);

    const filterColumns = filter(Object.keys(tableFilter),
      filterColumn => tableFilter[filterColumn].length);

    const tableData = filter(parsedCsv.data, row => {
      let matches = true;

      filterColumns.forEach(filterColumn => {
        const haystack = row[filterColumn].toString().toLowerCase();
        const needle = tableFilter[filterColumn].toLowerCase();

        matches = matches && includes(haystack, needle);
      });

      return matches;
    });

    return orderBy(tableData, orderColumns, orders);
  }
);

/**
 *  Get Table Columns Selector
 */
export const getTableColumns = createSelector(
  getParsedCsv,
  parsedCsv => parsedCsv.meta.fields
);

/**
 *  Get Map Markers Selector
 */
export const getMapMarkers = createSelector(
  getLatitudeColumn,
  getLongitudeColumn,
  getMarkerLabelColumn,
  getTableData,
  getHiddenRowIds,
  (latitudeColumn, longitudeColumn, markerLabelColumn, tableData, hiddenRowIds) => {
    const markers = [];
    tableData.forEach(row => {
      if (row[latitudeColumn] && row[longitudeColumn] && row[markerLabelColumn] && hiddenRowIds.indexOf(row.Id) === -1) {
        markers.push({
          lat: row[latitudeColumn],
          lng: row[longitudeColumn],
          label: row[markerLabelColumn],
        });
      }
    });
    return markers;
  }
);

/**
 *  Get Settings Selector
 */
export const getSettings = createSelector(
  getDelimiter,
  getLatitudeColumn,
  getLongitudeColumn,
  getMarkerLabelColumn,
  getTableColumns,
  (delimiter, latitudeColumn, longitudeColumn, markerLabelColumn, tableColumns) => {
    const columnOptions = tableColumns.map(columnName => ({
      name: columnName,
      value: columnName,
    }));

    return {
      delimiterOptions: [
        { name: 'Comma', value: ',' },
        { name: 'Semicolon', value: ';' },
        { name: 'Tab', value: '\t' },
      ],
      delimiter,
      latitudeColumnOptions: columnOptions,
      latitudeColumn,
      longitudeColumnOptions: columnOptions,
      longitudeColumn,
      markerLabelColumnOptions: columnOptions,
      markerLabelColumn,
    };
  }
);

/**
 *  Get Data Preview Page Data
 */
export const getDataPreviewPageData = createSelector(
  getCsv,
  getDelimiter,
  getLatitudeColumn,
  getLongitudeColumn,
  getMarkerLabelColumn,
  getHiddenRowIds,
  getTableOrder,
  getTableFilter,
  (csv, delimiter, latitudeColumn, longitudeColumn, markerLabelColumn, hiddenRowIds, tableOrder, tableFilters) => {
    const table = parseCsv(csv, delimiter);

    const orderedTableData = orderBy(table.data, [tableOrder.field], [tableOrder.direction]);

    // TODO Filter data

    return { ...table, ...{ data: orderedTableData }, latitudeColumn, longitudeColumn, markerLabelColumn, hiddenRowIds, tableOrder, tableFilters };
  }
);
