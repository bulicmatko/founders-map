/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change Latitude Column - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { LATITUDE_COLUMN_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change Latitude Column
 */
export default latitudeColumn => ({
  type: LATITUDE_COLUMN_CHANGE,
  payload: {
    latitudeColumn,
  },
});
