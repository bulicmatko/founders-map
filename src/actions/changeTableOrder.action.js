/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change Table Order - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { TABLE_ORDER_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change Table Order
 */
export default (tableOrder) => ({
  type: TABLE_ORDER_CHANGE,
  payload: {
    tableOrder,
  },
});
