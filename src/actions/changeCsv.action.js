/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change CSV - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { CSV_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change CSV
 */
export default csv => ({
  type: CSV_CHANGE,
  payload: {
    csv,
  },
});
