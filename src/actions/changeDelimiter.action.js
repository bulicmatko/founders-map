/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change Delimiter - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { DELIMITER_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change Delimiter
 */
export default delimiter => ({
  type: DELIMITER_CHANGE,
  payload: {
    delimiter,
  },
});
