/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change Row On Map Visability - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { ROW_ON_MAP_VISABILITY_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change Row On Map Visability
 */
export default hiddenRowIds => ({
  type: ROW_ON_MAP_VISABILITY_CHANGE,
  payload: {
    hiddenRowIds,
  },
});
