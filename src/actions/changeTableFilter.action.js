/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change Table Filter - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { TABLE_FILTER_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change Table Filter
 */
export default (tableFilter) => ({
  type: TABLE_FILTER_CHANGE,
  payload: {
    tableFilter,
  },
});
