/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change Longitude Column - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { LONGITUDE_COLUMN_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change Longitude Column
 */
export default longitudeColumn => ({
  type: LONGITUDE_COLUMN_CHANGE,
  payload: {
    longitudeColumn,
  },
});
