/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Change Marker Label Column - Action

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { MARKER_LABEL_COLUMN_CHANGE } from '../constants/actionTypes.const';

/**
 *  Change Marker Label Column
 */
export default markerLabelColumn => ({
  type: MARKER_LABEL_COLUMN_CHANGE,
  payload: {
    markerLabelColumn,
  },
});
