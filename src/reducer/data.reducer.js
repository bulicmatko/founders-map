/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Data - Reducer

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import {
  CSV_CHANGE,
  DELIMITER_CHANGE,
  LATITUDE_COLUMN_CHANGE,
  LONGITUDE_COLUMN_CHANGE,
  MARKER_LABEL_COLUMN_CHANGE,
  ROW_ON_MAP_VISABILITY_CHANGE,
  TABLE_ORDER_CHANGE,
  TABLE_FILTER_CHANGE,
} from '../constants/actionTypes.const';

import { saveState, getState } from '../utils/persistState.utils';

/**
 *  Initial State
 */
const initState = getState() || {
  csv: '',
  delimiter: ',',
  latitudeColumn: '',
  longitudeColumn: '',
  markerLabelColumn: '',
  hiddenRowIds: [],
  tableOrder: {},
  tableFilter: {},
};

/**
 *  Data - Reducer
 */
export default function (state = initState, action) {
  const { type, payload } = action;

  switch (type) {
    case CSV_CHANGE:
    case DELIMITER_CHANGE:
    case LATITUDE_COLUMN_CHANGE:
    case LONGITUDE_COLUMN_CHANGE:
    case MARKER_LABEL_COLUMN_CHANGE:
    case ROW_ON_MAP_VISABILITY_CHANGE:
    case TABLE_ORDER_CHANGE:
    case TABLE_FILTER_CHANGE:
      return saveState({ ...state, ...payload });
    default:
      return state;
  }
}
