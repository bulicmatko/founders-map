/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Reducer - Index

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { combineReducers } from 'redux';

import data from './data.reducer';

/**
 *  Combine Reducers
 */
export default combineReducers({
  data,
});
