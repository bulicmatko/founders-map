/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Route - Util

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/**
 *  Routes
 */
const routes = {
  root: '/',

  'data-import': '/data-import',
  'data-preview': '/data-preview',
};

/**
 *  Route
 */
export default name => routes[name];
