/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Parse CSV - Util

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import { parse } from 'papaparse';

/**
 *  Parse CSV
 */
export default function (csv, delimiter) {
  return parse(csv.trim(), { header: true, dynamicTyping: true, delimiter });
}
