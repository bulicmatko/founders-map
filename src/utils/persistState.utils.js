/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Persist state - Util

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

const { sessionStorage } = window;

/**
 *  Persist state
 */
export function saveState(state) {
  sessionStorage.setItem('state', JSON.stringify(state));

  return state;
}

/**
 *  Persist state
 */
export function getState() {
  const state = sessionStorage.getItem('state');

  return JSON.parse(state) || null;
}
