/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Root Container (Development)

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';

import DevTools from '../../components/dev/DevTools';
import GoogleMap from '../../components/shared/GoogleMap';

/**
 *  Root Container
 */
export default class extends Component {
  static displayName = 'RootContainer';

  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    children: null,
  };

  render() {
    const { children } = this.props;

    return (
      <div>
        <GoogleMap />
        {children}
        <DevTools />
      </div>
    );
  }
}
