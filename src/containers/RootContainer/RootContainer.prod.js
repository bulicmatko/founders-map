/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Root Container (Production)

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React, { Component, PropTypes } from 'react';

import GoogleMap from '../../components/shared/GoogleMap';

/**
 *  Root Container
 */
export default class extends Component {
  static displayName = 'RootContainer';

  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    children: null,
  };

  render() {
    const { children } = this.props;

    return (
      <div>
        <GoogleMap />
        {children}
      </div>
    );
  }
}
