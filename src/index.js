/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  App

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { blue500 } from 'material-ui/styles/colors';

import injectTapEventPlugin from 'react-tap-event-plugin';

import routes from './routes';
import store from './store';

import './styles/global';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: blue500,
  },
});

injectTapEventPlugin();

/**
 *  Render App
 */
render(
  <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store}>
      <Router history={hashHistory} routes={routes} />
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('App')
);
