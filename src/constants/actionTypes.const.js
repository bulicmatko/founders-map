/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Action Types - Constants

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/**
 *  Action Types
 */
export const CSV_CHANGE = 'CSV_CHANGE';
export const DELIMITER_CHANGE = 'DELIMITER_CHANGE';
export const LATITUDE_COLUMN_CHANGE = 'LATITUDE_COLUMN_CHANGE';
export const LONGITUDE_COLUMN_CHANGE = 'LONGITUDE_COLUMN_CHANGE';
export const MARKER_LABEL_COLUMN_CHANGE = 'MARKER_LABEL_COLUMN_CHANGE';
export const ROW_ON_MAP_VISABILITY_CHANGE = 'ROW_ON_MAP_VISABILITY_CHANGE';
export const TABLE_ORDER_CHANGE = 'TABLE_ORDER_CHANGE';
export const TABLE_FILTER_CHANGE = 'TABLE_FILTER_CHANGE';
