/* eslint-env browser */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Routes

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

import RootContainer from '../containers/RootContainer';

import HomePage from '../components/pages/HomePage';
import DataImportPage from '../components/pages/DataImportPage';
import DataPreviewPage from '../components/pages/DataPreviewPage';

import route from '../utils/route.util';

/**
 *  Routes
 */
export default {
  path: route('root'),
  component: RootContainer,
  indexRoute: {
    component: HomePage,
  },
  childRoutes: [
    {
      path: route('data-import'),
      component: DataImportPage,
    },
    {
      path: route('data-preview'),
      component: DataPreviewPage,
    },
  ],
};
