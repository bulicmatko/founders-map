/* eslint-env node */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  Server

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

const path = require('path');
const express = require('express');

const PUBLIC_DIR = path.resolve(__dirname, './public');

const PORT = 3000;
const HOST = 'localhost';

/**
 *  Server
 */
express()
  .use('/', express.static(PUBLIC_DIR))
  .listen(PORT, HOST, () => {
    console.log('Server running on http://%s:%d', HOST, PORT); // eslint-disable-line
  });
